package group.voidream.com.newstest.UtilClasses;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

import group.voidream.com.newstest.CustomObject.ListNews;
import group.voidream.com.newstest.Interfaces.OnList;
import group.voidream.com.newstest.R;

/**
 * Created by Ryvaldie I.H on 16/05/18.
 * Void Group.
 */
public class GetNews {

    private Context context;

    public GetNews(Context context) {
        this.context = context;
    }

    @SuppressLint("StaticFieldLeak")
    public class headlines extends AsyncTask<String, String, ArrayList<ListNews>> {

        private OnList onList;

        public headlines(OnList onList) {
            this.onList = onList;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            onList.Started();
        }

        @Override
        protected ArrayList<ListNews> doInBackground(String... strings) {
            ArrayList<ListNews> result = new ArrayList<>();
            try {
                String country = Locale.getDefault().getCountry().toLowerCase();
                String apikey = context.getString(R.string.api_key);
                URL url = new URL(context.getString(R.string.url_headlines, country, apikey));

                HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

                InputStream inputStream = connection.getInputStream();
                BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
                StringBuilder stringBuilder = new StringBuilder();
                String contents;
                while ((contents = in.readLine()) != null) {
                    stringBuilder.append(contents);
                }

                inputStream.close();
                connection.disconnect();

                JSONObject jsonObject = new JSONObject(stringBuilder.toString());
                if (jsonObject.getString("status").equals("ok")) {
                    int size = jsonObject.getInt("totalResults");
                    JSONArray articles = jsonObject.getJSONArray("articles");
                    for (int a = 0; a < size; a++) {
                        JSONObject article = articles.getJSONObject(a);
                        String details_url = article.getString("url");
                        String image_url = article.getString("urlToImage");
                        String title = article.getString("title");
                        String description = article.getString("description");
                        result.add(new ListNews(details_url, image_url, title, description));
                    }
                } else {
                    return null;
                }

            } catch (NullPointerException | IOException | JSONException e) {
                Log.e(context.getClass().getSimpleName(), e.getMessage());
                return null;
            }

            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<ListNews> result) {
            onList.Completed(result);
        }

    }

}
