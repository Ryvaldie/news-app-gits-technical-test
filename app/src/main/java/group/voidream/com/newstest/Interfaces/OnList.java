package group.voidream.com.newstest.Interfaces;

import java.util.ArrayList;

import group.voidream.com.newstest.CustomObject.ListNews;

/**
 * Created by Ryvaldie I.H on 16/05/18.
 * Void Group.
 */
public interface OnList {

    void Completed(ArrayList<ListNews> result);

    void Started();

}
