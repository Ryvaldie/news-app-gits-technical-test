package group.voidream.com.newstest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import group.voidream.com.newstest.CustomObject.ListNews;

public class NewsDetailActivity extends AppCompatActivity {

    private int position;
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);

        final ArrayList<ListNews> result = MainActivity.result;
        position = getIntent().getIntExtra("position", 0) - 1;

        webView = findViewById(R.id.webView);
        webView.setWebViewClient(new WebViewClient());
        final TextView title = findViewById(R.id.textView_title);
        final TextView url = findViewById(R.id.textView_address);

        if (result != null) {
            webView.loadUrl(result.get(position).getDetails_url());
            title.setText(result.get(position).getTitle());
            url.setText(result.get(position).getDetails_url());
        }

        ImageButton back = findViewById(R.id.imageButton_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position--;
                if (position < 0) {
                    finish();
                } else if (result != null) {
                    webView.loadUrl(result.get(position).getDetails_url());
                    title.setText(result.get(position).getTitle());
                    url.setText(result.get(position).getDetails_url());
                }
            }
        });
        ImageButton forward = findViewById(R.id.imageButton_forward);
        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position++;
                if (position >= (result != null ? result.size() : 0)) {
                    finish();
                } else if (result != null) {
                    webView.loadUrl(result.get(position).getDetails_url());
                    title.setText(result.get(position).getTitle());
                    url.setText(result.get(position).getDetails_url());
                }
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        webView.clearCache(true);
        webView.clearHistory();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.flip, R.anim.flop);
    }

}
