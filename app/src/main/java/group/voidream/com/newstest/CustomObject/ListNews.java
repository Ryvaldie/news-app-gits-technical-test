package group.voidream.com.newstest.CustomObject;

/**
 * Created by Ryvaldie I.H on 16/05/18.
 * Void Group.
 */
public class ListNews {

    public String getImage_url() {
        return image_url;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getDetails_url() {
        return details_url;
    }

    private String details_url;
    private String image_url;
    private String title;
    private String description;

    public ListNews(String details_url, String image_url, String title, String description) {
        this.details_url = details_url;
        this.image_url = image_url;
        this.title = title;
        this.description = description;
    }

}
