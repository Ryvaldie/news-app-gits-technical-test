package group.voidream.com.newstest.AdapterClasses;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import group.voidream.com.newstest.CustomObject.ListNews;
import group.voidream.com.newstest.R;

/**
 * Created by Ryvaldie I.H on 16/05/18.
 * Void Group.
 */
public class CustomList extends BaseAdapter {

    private ArrayList<ListNews> listNews;
    private Activity activity;

    public CustomList(Activity activity) {
        this.activity = activity;
    }

    public void updateData(ArrayList<ListNews> listNews) {
        this.listNews = listNews;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return listNews != null ? listNews.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return listNews.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    class ViewHolder {
        ImageView news_image;
        TextView title;
        TextView detail;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = activity.getLayoutInflater().inflate(R.layout.custom_list, null);
        }

        ViewHolder viewHolder = new ViewHolder();
        viewHolder.news_image = view.findViewById(R.id.news_image);
        Picasso.get()
                .load(Uri.parse(listNews.get(i)
                .getImage_url()))
                .resize(360, 120)
                .centerCrop()
                .into(viewHolder.news_image);

        viewHolder.title = view.findViewById(R.id.title);
        viewHolder.title.setText(listNews.get(i).getTitle());

        viewHolder.detail = view.findViewById(R.id.detail);
        viewHolder.detail.setText(listNews.get(i).getDescription());

        return view;
    }

}
