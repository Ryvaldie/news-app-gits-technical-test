package group.voidream.com.newstest;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import group.voidream.com.newstest.AdapterClasses.CustomHeadlines;
import group.voidream.com.newstest.AdapterClasses.CustomList;
import group.voidream.com.newstest.CustomObject.ListNews;
import group.voidream.com.newstest.Interfaces.OnList;
import group.voidream.com.newstest.UtilClasses.GetNews;

public class MainActivity extends AppCompatActivity implements
        SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemClickListener {

    private CustomHeadlines customHeadlines;
    private CustomList customList;
    private GetNews getNews;
    private SwipeRefreshLayout refreshLayout;
    private TextView refreshIcon;
    private ViewPager viewPager;
    private ListView news_list;
    private View header_view;
    private int ads_loop = 0;
    public static ArrayList<ListNews> result;

    @SuppressLint("InflateParams")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        customHeadlines = new CustomHeadlines(this);
        customList = new CustomList(this);
        getNews = new GetNews(this);

        refreshIcon = findViewById(R.id.refreshIcon);
        refreshIcon.setVisibility(View.GONE);
        refreshLayout = findViewById(R.id.refreshLayout);
        refreshLayout.setOnRefreshListener(this);

        header_view = getLayoutInflater().inflate(R.layout.header, null);
        viewPager = header_view.findViewById(R.id.pager);
        viewPager.setAdapter(customHeadlines);
        news_list = findViewById(R.id.news_list);
        news_list.addHeaderView(header_view);
        news_list.setAdapter(customList);
        news_list.setOnItemClickListener(this);
        getNews.new headlines(onList).execute();

    }

    private Thread loopHeadlines = new Thread(new Runnable() {
        @SuppressWarnings("InfiniteLoopStatement")
        @Override
        public void run() {
            while (true) {
                int pages_size = customHeadlines.getCount();
                if (pages_size != 0) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            viewPager.setCurrentItem(ads_loop, true);
                        }
                    });
                    try {
                        TimeUnit.SECONDS.sleep(4);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (ads_loop == pages_size - 1) {
                        ads_loop = 0;
                    } else {
                        ads_loop++;
                    }
                }
            }
        }
    });

    private OnList onList = new OnList() {
        @Override
        public void Completed(ArrayList<ListNews> result) {
            MainActivity.result = result;
            if (result == null) {
                news_list.removeHeaderView(header_view);
                refreshIcon.setVisibility(View.VISIBLE);
            } else {
                news_list.addHeaderView(header_view);
                customList.updateData(result);
                customHeadlines.updateData(result);
                if (!loopHeadlines.isAlive()) {
                    loopHeadlines.start();
                }
            }
            refreshLayout.setRefreshing(false);
        }

        @Override
        public void Started() {
            news_list.removeHeaderView(header_view);
            refreshIcon.setVisibility(View.GONE);
            refreshLayout.setRefreshing(true);
        }
    };

    @Override
    public void onRefresh() {
        getNews.new headlines(onList).execute();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent news_details = new Intent(this, NewsDetailActivity.class);
        news_details.putExtra("position", i);
        startActivity(news_details);
        overridePendingTransition(R.anim.flip, R.anim.flop);
    }

}
