package group.voidream.com.newstest.AdapterClasses;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import group.voidream.com.newstest.CustomObject.ListNews;
import group.voidream.com.newstest.NewsDetailActivity;
import group.voidream.com.newstest.R;

/**
 * Created by Ryvaldie I.H on 16/05/18.
 * Void Group.
 */
public class CustomHeadlines extends PagerAdapter implements View.OnClickListener {

    private ArrayList<ListNews> news;
    private Activity activity;
    private int position;

    public CustomHeadlines(Activity activity) {
        this.activity = activity;
    }

    public void updateData(ArrayList<ListNews> news) {
        this.news = news;
        notifyDataSetChanged();
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return news == null ? 0 : news.size() > 5 ? 5 : news.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup viewGroup, int position) {
        @SuppressLint("InflateParams")
        View view = activity.getLayoutInflater().inflate(R.layout.sliding_image, null);
        assert view != null;

        ImageView imageView = view.findViewById(R.id.image);
        Picasso.get()
                .load(Uri.parse(news.get(position).getImage_url()))
                .into(imageView);

        TextView title = view.findViewById(R.id.title);
        title.setText(news.get(position).getTitle());

        view.setOnClickListener(this);
        this.position = position;

        viewGroup.addView(view, 0);

        return view;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {

    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public void onClick(View view) {
        Intent news_details = new Intent(activity, NewsDetailActivity.class);
        news_details.putExtra("position", position);
        activity.startActivity(news_details);
    }
}
